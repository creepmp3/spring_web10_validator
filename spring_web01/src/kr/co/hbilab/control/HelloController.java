package kr.co.hbilab.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * 2015. 5. 28.
 * @author user
 * 
 */
public class HelloController implements Controller{

    @Override
    public ModelAndView handleRequest(HttpServletRequest req, HttpServletResponse resp)
            throws Exception {
        
        // Model : data
        // View : 찾아갈 view 의 이름
        ModelAndView mav = new ModelAndView();
        // req.setAttribute("msg", "안녕하세요");
        mav.addObject("msg", "안녕하세요 Spring World!!");
        mav.setViewName("hello");
        return mav;
    }
    
}
