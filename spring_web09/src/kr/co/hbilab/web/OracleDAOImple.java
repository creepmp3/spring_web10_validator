package kr.co.hbilab.web;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 * 2015. 6. 2.
 * @author KDY
 */
public class OracleDAOImple implements Dao{
    private JdbcTemplate jdbctemplate;
    StringBuffer sql = new StringBuffer();
    
    public void setJdbctemplate(JdbcTemplate jdbctemplate) {
        this.jdbctemplate = jdbctemplate;
    }

    @Override
    public List<MemDTO> selectAll() {
        RowMapper rm = new RowMapper(){
            @Override
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                String id = rs.getString("id");
                String pw = rs.getString("pw");
                String email = rs.getString("email");
                MemDTO dto = new MemDTO();
                dto.setId(id);
                dto.setPw(pw);
                dto.setEmail(email);
                return dto;
            }
        };
        sql.setLength(0);
        sql.append("\n SELECT * FROM MEM ");
        List<MemDTO> list = jdbctemplate.query(sql.toString(), rm);
        return list;
    }
    
    @Override
    public boolean loginChk(String id, String pw){
        int cnt = 0;
        
        sql.setLength(0);
        sql.append("\n SELECT id, pw              ");
        sql.append("\n      , COUNT(*) OVER() cnt ");
        sql.append("\n   FROM MEM                 ");
        sql.append("\n  WHERE id = ?              ");
        sql.append("\n    AND pw = ?              ");
        
        RowMapper rm = new RowMapper(){
            @Override
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                int cnt = rs.getInt("cnt");
                String id = rs.getString("id");
                String pw = rs.getString("pw");
                MemDTO dto = new MemDTO();
                dto.setId(id);
                dto.setPw(pw);
                return dto;
            }
        };
        MemDTO dto = null;
        
        try{
            dto = (MemDTO) jdbctemplate.queryForObject(sql.toString(), rm, id, pw);
        }catch(Exception e){
            
        }
        if(dto!=null) return true;
        else return false;
    }

    
}
