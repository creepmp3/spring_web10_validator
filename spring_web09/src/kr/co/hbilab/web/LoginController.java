package kr.co.hbilab.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 2015. 6. 2.
 * @author KDY
 */

@Controller
//@RequestMapping(value="/member")
public class LoginController {
    
    @Autowired
    Dao dao;
    
/*
    @RequestMapping("/login")
    public String login(){
        return "/login/login";
    }
    페이지만 이동할땐
    spring-ctx.xml에 mvc:view-controller로 처리
    , method=RequestMethod.POST
    */
    
    @RequestMapping("/login")
    public String login(Model model, HttpServletRequest req){
        HttpSession session = req.getSession();
        session.setAttribute("url", req.getRequestURI());
        return "/member/login";
    }
    
    @RequestMapping(value="/loginOk")
    public String loginOk(Model model, @ModelAttribute("mem") MemDTO dto, HttpServletRequest req){
        boolean loginChk = dao.loginChk(dto.getId(), dto.getPw());
        HttpSession session = req.getSession();
        session.setAttribute("url", req.getRequestURI());
        if(loginChk){
            session.setAttribute("id", dto.getId());
            return "redirect:/main";
        }else{
            model.addAttribute("msg", "로그인실패");
            return "redirect:/member/login";
        }
    }
    
    @RequestMapping(value="/logout")
    public String logOut(HttpServletRequest req){
        req.getSession().invalidate();
        return "redirect:/main";
    }
}
